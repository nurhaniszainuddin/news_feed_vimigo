<?php

use Faker\Generator as Faker;

$factory->define(App\Comments::class, function (Faker $faker) {
    return [
        'post_id' => $faker->numberBetween($min = 1, $max = 50),
        'comment' => $faker->realText($maxNbChars = 100, $indexSize = 1)
    ];
});
