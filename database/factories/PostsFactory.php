<?php

use Faker\Generator as Faker;

$factory->define(App\Posts::class, function ($faker) {
    return [
        'user_id' => $faker->numberBetween($min = 1, $max = 100),
        'user_name' => $faker->name,
        'author_id' => $faker->optional($weight = 0.3, $default = NULL)->numberBetween($min = 1, $max = 200),
        'category' => $faker->randomElement(['c1', 'c2', 'c3', 'c4', 'c5']),
        'content' => $faker->optional($weight = 0.5, $default = $faker->realText($maxNbChars = 100, $indexSize = 1))->realText($maxNbChars = 200, $indexSize = 2),
        'media_path' => $faker->optional($weight = 0.8, $default = NULL)->image('public/img/mediapost',400,300, null, false),
        'status' => 1,
        'like_count' => $faker->numberBetween($min = 0, $max = 300),
        'share_count' => $faker->numberBetween($min = 0, $max = 300)
    ];
});
