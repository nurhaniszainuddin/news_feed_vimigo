<?php

use Faker\Generator as Faker;

$factory->define(App\EventLog::class, function (Faker $faker) {
    return [
        'user_id' => $faker->numberBetween($min = 1, $max = 100),
        'post_id'=> $faker->optional($weight = 0.8, $default = NULL)->numberBetween($min = 1, $max = 50),
        'description'=> $faker->randomElement(['Person A created a post.', "Person A like Person B's post.", 'Person A shared a post.',"Good day! Enjoy your day with our new feature. Let's check it out!"]),
        'category'=> $faker->randomElement(['c1', 'c2', 'c3', 'c4', 'c5',''])
    ];
});
