<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'NewsFeedController@index')->name('newsfeed')->middleware('auth');

/** Post Start */
Route::get('/mypost', 'PostController@fn_get_user_posts')->name('mypost')->middleware('auth');
Route::get('/createpost', 'PostController@fn_create_post')->middleware('auth');
Route::post('/storepost', 'PostController@fn_store_post')->middleware('auth');
Route::get('/post/{id}', 'PostController@fn_show_post')->middleware('auth');
Route::get('/editpost/{id}', 'PostController@fn_edit_post')->middleware('auth');
Route::post('/updatepost/{id}', 'PostController@fn_update_post')->middleware('auth');
Route::get('/deletepost/{id}', 'PostController@fn_destroy_post')->middleware('auth');
Route::get('/countevent/{id}/{event}', 'PostController@fn_update_post_event_count')->middleware('auth');
Route::get('/sharepost/{id}', 'PostController@fn_share_post')->middleware('auth');
/** Post End */

/** QTable Start */
Route::get('/updateqvalue/{id}/{category}', 'QTableController@fn_update_qvalue')->middleware('auth');
/** QTable End */

/** Comment Start */
Route::get('/comments/{id}', 'CommentController@fn_get_comment_list')->middleware('auth');
Route::get('/storecomment/{id}/{comment}', 'CommentController@fn_store_comment')->middleware('auth');
Route::get('/editcomment/{id}', 'CommentController@fn_edit_comment')->middleware('auth');
Route::post('/updatecomment/{id}', 'CommentController@fn_update_comment')->middleware('auth');
Route::get('/deletecomment/{id}', 'CommentController@fn_destroy_comment')->middleware('auth');
/** Comment End */

/** Event Log Start */
Route::get('/eventlogs/{author}/{event}', 'EventLogsController@fn_store_event')->middleware('auth');
/** Event Log End */

/** News Feed Start */
Route::get('/newsfeed', 'NewsFeedController@index')->name('newsfeed')->middleware('auth');
/** News Feed End */