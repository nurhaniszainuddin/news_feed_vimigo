<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Followers extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'followers';

    protected $fillable = [
        'id',
        'user_id',
        'ref_user_id'
    ];
}
