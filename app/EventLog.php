<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EventLog extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'event_logs';

    protected $fillable = [
        'id',
        'user_id',
        'post_id',
        'description',
        'category'
    ];
}
