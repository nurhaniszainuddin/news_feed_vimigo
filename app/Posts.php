<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Posts extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'posts';

    protected $fillable = [
        'id',
        'user_id',
        'user_name',
        'author_id',
        'category',
        'content',
        'media_path',
        'status',
        'like_count',
        'share_count'
    ];
}
