<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Posts;
use App\Comments;
use App\EventLog;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\QTableController;

class PostController extends EventLogsController
{
    /**
     * Display a listing of the user post.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function fn_get_user_posts()
    {
        $post_list = Posts::where('user_id', Auth::user()->id)
        ->orderBy('created_at','desc')
        ->get();
        return response()->view('post.listpost', ['data' => $post_list]);
    }

    /**
     * Show the form for creating a new post.
     * 
     * @return \Illuminate\Http\Response
     */
    public function fn_create_post()
    {
        return response()->view('createpost');
    }

    /**
     * Store a newly created post in database.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function fn_store_post(Request $request)
    {
        $path = '';
        $path = $request->file('mediapost');
        if($path){
           $path = $path->store('mediapost');
        }

        $create_post = new Posts();
        $create_post = $create_post->create([
            'user_id' => Auth::user()->id,
            'user_name' => Auth::user()->name,
            'category' => strval($request['category']),
            'content' => strval($request['content']),
            'media_path' => str_replace("mediapost/","",$path)
        ]);

        // $category ='c'.rand(1,5);
        if($create_post){
            $data = [
                'user_id'=>Auth::user()->id,
                'post_id'=>$create_post->id,
                'author'=>Auth::user()->name,
                'event'=>1,
                'category'=>$create_post->category
            ];
            $aa = $this->fn_store_event($data);
            
            return redirect()->route('newsfeed');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function fn_show_post($id)
    {
        $post = Posts::find($id);
        if(!empty($post)){
            $qtable_controller = new QTableController;
            if($qtable_controller->fn_update_qvalue(Auth::user()->id,$post->category)){
                return response()->view('post.post', ['data' => $post]);
            }
        }
        
        return response()->view('post.post', ['data' => $post]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function fn_edit_post($id)
    {
        $post = Post::find($id);
        $data = ['post'=>$post];
        
        return $data;
    }

    /**
     * Update the specified post info in database.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function fn_update_post(Request $request,$id)
    {
        $post = Post::find($id);
        $media_list = $request->file('media');
        $media = 0;
        if(($post->media == 1 AND sizeof($media_list) < 0) OR (sizeof($media_list) > 0)){$media = 1;}

        $update_post = $post->update([
            'category' => strval($request['category']),
            'content' => strval($request['content']),
            'media' => $media
            ]
        );

        if (sizeof(media_list) > 0) {
            $i = 0;
            foreach($media_list as $m){
                $path = $m->store('mediapost');
                $create_media = new PostXMedia();
                $create_media = $create_media->create([
                    'post_id' => $create_post->id,
                    'index' => $i,
                    'path' => strval($path)
                ]);
                $i++;
            }
        }
        $status = 'Failed';
        if($update_post){$status='Success';}
        $data = [
            'route'=> strval('mypost'),//return redirect()->route('mypost');
            'status'=> $status
        ];
        return $data;  
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function fn_destroy_post($id)
    {
        $post = Posts::find($id);
        if($post){
            $comment_list = Comments::where('post_id',$id)->get();
            foreach($comment_list as $comment){$comment->delete();}

            $event_list = EventLog::where('post_id',$id)->get();
            foreach($event_list as $event){$event->delete();}
        }
        $post->delete();

        $status = 'Failed';
        if($post){$status='Success';}
        $data = [
            'status'=> $status
        ];
        return $data;
    
    }

    /**
     * Update the post like or share count in database.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function fn_update_post_event_count($id,$event)
    {
        $post = Posts::find($id);
        $tmp = $post->$event;
        $update_post = $post->update([
            strval($event) => $post->$event + 1
        ]);
        $status = 'Success';
        if($post->$event == $tmp){$status = 'Failed';}
        $data = [
            strval($event)=>$post->$event,
            'status'=>$status
        ];
        return $data;
    }

    /**
     * Update the post like or share count in database.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function fn_share_post($id)
    {
        $post = Posts::find($id);
        $create_post = new Posts();
        $create_post = $create_post->create([
            'user_id' => Auth::user()->id,
            'user_name' => Auth::user()->name,
            'category' => $post->category,
            'content' => $post->content,
            'media_path' => $post->media_path
        ]);
        $status = 'Failed';
        

        if($create_post){
            $status = 'Success';
            $data = [
                'user_id'=>Auth::user()->id,
                'post_id'=>$create_post->id,
                'author'=>Auth::user()->name,
                'event'=>3,
                'category'=>$create_post->category
            ];
            $this->fn_store_event($data);
        }
        $res = ['status'=>$status];
        return $res;
        
    }

}
