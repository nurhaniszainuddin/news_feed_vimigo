<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Comments;

class CommentController extends Controller
{
    /**
     * Display comment list of a post.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function fn_get_comment_list($id)
    {
        $comment_list = Comments::where('post_id', $id)->get();
        $data = [
            'comment_list'=>$comment_list,
            'status'=>'Success'
        ];
        return $data;

        $users = DB::table('users')
            ->join('contacts', 'users.id', '=', 'contacts.user_id')
            ->join('orders', 'users.id', '=', 'orders.user_id')
            ->select('users.*', 'contacts.phone', 'orders.price')
            ->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function fn_store_comment($id,$comment)
    {
        $create_comment = new Comments();
        $create_comment = $create_comment->create([
            'post_id' => $id,
            'comment' => $comment
        ]);

        $status = 'Failed';
        if($create_comment){$status = 'Success';}
        $data = [
            'comment' => $create_comment->comment,
            'status' => $status
        ];
        return $data;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function fn_update_comment(Request $request, $id)
    {
        $comment = Comments::find($id);
        $update_comment = $comment->update([
            'comment' => $request['comment']
        ]);

        $status = 'Success';
        if($update_comment == 1){$status = 'Failed';}

        $data = [
            'comment' => $comment->comment,
            'status' => $status
        ];
        return $data;

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function fn_edit_comment($id)
    {
        $comment = Comments::find($id);
        $data = ['comment'=>$comment];
        return $data;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function fn_destroy_comment($id)
    {
        $comment = Comments::find($id);
        $comment->delete();
        $status = 'Success';
        if($comment){$status = 'Failed';}
        $data = ['status'=>$status];
        return $data;
    }
}
