<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\EventLog;
use App\Posts;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class NewsFeedController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $qvalue_list=[];
        // for($i=0;$i<=5;$i++){
        //     $cat = strval('c'.$i);
        //     $qvalue_list['c'.$i] = Auth::user()->$cat;
        // }

        // return $qvalue_list;
               
        // return Carbon::today();
        // ->paginate(7);
        $date = new \DateTime();
        $date->modify('-24 hours');
        $formatted_date = $date->format('Y-m-d H:i:s');
        $data = Posts::where('created_at', '>',$formatted_date)
        ->orderBy('created_at', 'desc')
        ->get();
        // foreach($search as $field=>$value)
        // {
        //     $cat_activities = $cat_activities->where('category', Auth::user()->last_category);        
        // }
        // ->orderBy('created_at','desc')
        // ->get();
        
        // $odd = array_filter($activity->toArray(), function ($key,$value) {return !($key% 2 == 0);});
        // $even = array_filter($activity->toArray(), function ($input) {$tmp_array=[]; foreach($input as $key => $value) return $key&1;});
        
        return response()->view('newsfeed', ['data' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
