<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\EventLog;
use App\Posts;
use App\User; 

class EventLogsController extends Controller
{

    /**
     * Store a newly created resource in storage.
     *
     * 1 : Create post
     * 2 : Like post
     * 3 : Share post
     * 
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function fn_store_event($data)
    {
        $post = Posts::find($data['post_id']);
        $post_owner = User::find($post->user_id);
        $desc = '';
        if($data['event'] == 1){
            $desc = $data['author'].' created a post.';
        }elseif($data['event'] == 2){
            $desc = $data['author'].' like '.$post_owner->name.' post';
        }

        
        $event = new EventLog();
        $event = $event->create([
            'user_id' => $data['user_id'],
            'post_id' => $post->id,
            'description' => $desc,
            'category' => $data['category']
        ]);

        $status = 'Failed';
        if($event){$status = 'Success';}
        return $status;
    }
}
