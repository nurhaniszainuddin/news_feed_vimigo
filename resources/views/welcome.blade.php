<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>
                        <a href="{{ route('register') }}">Register</a>
                    @endauth
                </div>
            @endif

            <div class="content">
                <div class="title m-b-md">
                    Laravel
                </div>

                <div class="links">
                    <a href="https://laravel.com/docs">Documentation</a>
                    <a href="https://laracasts.com">Laracasts</a>
                    <a href="https://laravel-news.com">News</a>
                    <a href="https://forge.laravel.com">Forge</a>
                    <a href="https://github.com/laravel/laravel">GitHub</a>
                </div>
            </div>
        </div>
    </body>
</html>



@extends('layouts.app')

@section('content')
<div class="container">
      
    <div class="wrapper">
        <div class="box">
            <div class="row row-offcanvas row-offcanvas-left">
                <!-- main right col -->
                <div class="column col-sm-10 col-xs-11 col-lg-12" id="main">
                    
                    <!-- top nav -->
                    <div class="navbar navbar-blue navbar-static-top">  
                        <div class="navbar-header">
                          <button class="navbar-toggle" type="button" data-toggle="collapse" data-target=".navbar-collapse">
                            <span class="sr-only">Toggle</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                          </button>
                          <a href="http://usebootstrap.com/theme/facebook" class="navbar-brand logo">b</a>
                        </div>
                        <nav class="collapse navbar-collapse" role="navigation">
                        <form class="navbar-form navbar-left">
                            <div class="input-group input-group-sm" style="max-width:360px;">
                              <input class="form-control" placeholder="Search" name="srch-term" id="srch-term" type="text">
                              <div class="input-group-btn">
                                <button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></button>
                              </div>
                            </div>
                        </form>
                        <ul class="nav navbar-nav">
                          <li>
                            <a href="#"><i class="glyphicon glyphicon-home"></i> Home</a>
                          </li>
                          <li>
                            <a href="#postModal" role="button" data-toggle="modal"><i class="glyphicon glyphicon-plus"></i> Post</a>
                          </li>
                          <li>
                            <a href="#"><span class="badge">badge</span></a>
                          </li>
                        </ul>
                        <ul class="nav navbar-nav navbar-right">
                          <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="glyphicon glyphicon-user"></i></a>
                            <ul class="dropdown-menu">
                              <li><a href="">More</a></li>
                              <li><a href="">More</a></li>
                              <li><a href="">More</a></li>
                              <li><a href="">More</a></li>
                              <li><a href="">More</a></li>
                            </ul>
                          </li>
                        </ul>
                        </nav>
                    </div>
                    <!-- /top nav -->
                  
                    <div class="padding">
                        <div class="full col-sm-9">
                           
                            <!-- content -->                      
                            <div class="row centerContent">
                              
                             <!-- main col left --> 
                             <div class="col-sm-5">
                              <div id="updateStatus" class="well"> 
                                <form class="form-horizontal" method="post" action="/storepost" enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                 <h4>What's New</h4>
                                 
                                  <div class="form-group" style="padding:14px;">
                                   <textarea class="form-control" placeholder="Update your status" id="content" name="content">{{old('content')}}</textarea>
                                 </div>
                                 <button class="btn btn-primary pull-right marBtn" type="submit" type="button">Post</button>
                                 <label class="btn btn-primary pull-right marBtn">
                                  Upload Picture
                                <input id="uploadPhoto" name="mediapost" type="file" style="display: none;">
                                </label><ul class="list-inline"><li></li><li></li><li></li></ul>
                                <img id="preview-image" src="img/150x150.gif" class=" pull-left img-preview noPreview"> 
                               </form>
                           </div>   
                             
                                    {{-- post with picture --------------------------------------- --}}
                                    <div class="panel panel-default">
                                        <div class="panel-thumbnail"><img src="img/bg_5.jpg" class="img-responsive image-wrapper"></div>
                                        <div class="panel-body">
                                            <div class="flexClss">
                                                <img class="profilePic" src="img/150x150.gif" class="img-circle pull-left"> 
                                                <h4>Raayarn Ismail</h4>
    
                                             </div>
                                          
                                          <hr>
                                            <form>
                                            <div class="input-group">
                                              <div class="input-group-btn">
                                              <button class="btn btn-default">+1</button><button class="btn btn-default"><i class="glyphicon glyphicon-share"></i></button>
                                              </div>
                                              <input class="form-control" placeholder="Add a comment.." type="text">
                                            </div>
                                            </form>
                                        </div>
                                      </div>
                                      {{-- end post with picture -------------------------------------------- --}}
                                      {{-- Post with status -------------------------------- --}}
                                      <div class="panel panel-default">
                                        {{-- <div class="panel-heading"> <h4>Stackoverflow</h4></div> --}}
                                         <div class="panel-body">
                                             <div class="flexClss">
                                                <img class="profilePic" src="img/150x150.gif" class="img-circle pull-left"> 
                                                <h4>Roslan Armina</h4>
    
                                             </div>
                                           
                                           <div class="clearfix"></div>
                                           <hr>
                                           
                                           <p>Saya ini hanya lah manusia biasa, Bekerja mencari Rezeki, ingin memberi mkan untuk anak dan isteri</p>
                                           
                                           <hr>
                                           <form>
                                           <div class="input-group">
                                             <div class="input-group-btn">
                                             <button class="btn btn-default">+1</button><button class="btn btn-default"><i class="glyphicon glyphicon-share"></i></button>
                                             </div>
                                             <input class="form-control" placeholder="Add a comment.." type="text">
                                           </div>
                                           </form>
                                           
                                         </div>
                                      </div>
                                      {{-- end post with status -------------------------------- --}}
                                      {{-- Post with status -------------------------------- --}}
                                      <div class="panel panel-default">
                                        {{-- <div class="panel-heading"> <h4>Stackoverflow</h4></div> --}}
                                         <div class="panel-body">
                                             <div class="flexClss">
                                                <img class="profilePic" src="img/150x150.gif" class="img-circle pull-left"> 
                                                <h4>Roslan Armina</h4>
    
                                             </div>
                                           
                                           <div class="clearfix"></div>
                                           <hr>
                                           
                                           <p>Saya ini hanya lah manusia biasa, Bekerja mencari Rezeki, ingin memberi mkan untuk anak dan isteri</p>
                                           
                                           <hr>
                                           <form>
                                           <div class="input-group">
                                             <div class="input-group-btn">
                                             <button class="btn btn-default">+1</button><button class="btn btn-default"><i class="glyphicon glyphicon-share"></i></button>
                                             </div>
                                             <input class="form-control" placeholder="Add a comment.." type="text">
                                           </div>
                                           </form>
                                           
                                         </div>
                                      </div>
                                      {{-- end post with status -------------------------------- --}}
                                      {{-- Post with status -------------------------------- --}}
                                      <div class="panel panel-default">
                                        {{-- <div class="panel-heading"> <h4>Stackoverflow</h4></div> --}}
                                         <div class="panel-body">
                                             <div class="flexClss">
                                                <img class="profilePic" src="img/150x150.gif" class="img-circle pull-left"> 
                                                <h4>Roslan Armina</h4>
    
                                             </div>
                                           
                                           <div class="clearfix"></div>
                                           <hr>
                                           
                                           <p>Saya ini hanya lah manusia biasa, Bekerja mencari Rezeki, ingin memberi mkan untuk anak dan isteri</p>
                                           
                                           <hr>
                                           <form>
                                           <div class="input-group">
                                             <div class="input-group-btn">
                                             <button class="btn btn-default">+1</button><button class="btn btn-default"><i class="glyphicon glyphicon-share"></i></button>
                                             </div>
                                             <input class="form-control" placeholder="Add a comment.." type="text">
                                           </div>
                                           </form>
                                           
                                         </div>
                                      </div>
                                      {{-- end post with status -------------------------------- --}}
                  {{-- post with picture --------------------------------------- --}}
                  <div class="panel panel-default">
                    <div class="panel-thumbnail"><img src="img/bg_5.jpg" class="img-responsive image-wrapper"></div>
                    <div class="panel-body">
                        <div class="flexClss">
                            <img class="profilePic" src="img/150x150.gif" class="img-circle pull-left"> 
                            <h4>Raayarn Ismail</h4>
    
                         </div>
                      
                      <hr>
                        <form>
                        <div class="input-group">
                          <div class="input-group-btn">
                          <button class="btn btn-default">+1</button><button class="btn btn-default"><i class="glyphicon glyphicon-share"></i></button>
                          </div>
                          <input class="form-control" placeholder="Add a comment.." type="text">
                        </div>
                        </form>
                    </div>
                  </div>
                  {{-- end post with picture -------------------------------------------- --}}
                                   
                </div>
                                  
                <!-- main col right -->
                <div class="col-sm-5">
                
                @foreach ($data['post_list'] as $post)
                    @if($post->media_path)
                        {{-- post with picture --------------------------------------- --}}
                        <div class="panel panel-default" id="post{{$post->id}}">
                          @if($post->media_path)
                            <div class="panel-thumbnail"><img src="{{asset("/img/mediapost/".$post->media_path) }}" class="img-responsive image-wrapper"></div>
                          @endif
                          <div class="panel-body">
                              <div class="flexClss">
                                  <img class="profilePic" src="img/150x150.gif" class="img-circle pull-left"> 
                                  <h4>{{$post->user_name}}</h4>
                                  <a class="edit-post" data-target="#postModal" role="button" data-post={{$post->content}} data-toggle="modal" style="border-block: 1-x;border-block-color: black"><i class="glyphicon glyphicon-pencil"></i> Post</a>
                                <button class="btn btn-danger delete-post" data-id="{{$post->id}}">Delete post</button>
                              </div>
                              <div class="clearfix"></div>
                                <hr>
                              <p>{{$post->content}}</p>
                          <hr>
                          <form>
                            <div class="input-group">
                              <div class="input-group-btn">
                              <button class="btn btn-default likePost" disabled>
                                
                                <svg width="0.7em" height="0.7em" viewBox="0 0 16 16" class="bi bi-heart-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                  <path fill-rule="evenodd" d="M8 1.314C12.438-3.248 23.534 4.735 8 15-7.534 4.736 3.562-3.248 8 1.314z"/>
                                </svg>
                                  <p>
                                    @if($post->like_count > 0)
                                      {{$post->like_count}}
                                    @endif
                                  </p>  
                              </button>
                              </div>
                              <input class="form-control" placeholder="Add a comment.." type="text">
                            </div>
                            </form>
                          </div>
                      </div>
                        {{-- end post with picture -------------------------------------------- --}}
                    @endif
    
                @endforeach
                
                                      
                  {{-- post with picture --------------------------------------- --}}
                  <div class="panel panel-default">
                    <div class="panel-thumbnail"><img src="img/bg_5.jpg" class="img-responsive"></div>
                    <div class="panel-body">
                        <div class="flexClss">
                            <img class="profilePic" src="img/150x150.gif" class="img-circle pull-left"> 
                            <h4>Raayarn Ismail</h4>
    
                         </div>
                     
                      <hr>
                        <form>
                        <div class="input-group">
                          <div class="input-group-btn">
                          <button class="btn btn-default">+1</button><button class="btn btn-default"><i class="glyphicon glyphicon-share"></i></button>
                          </div>
                          <input class="form-control" placeholder="Add a comment.." type="text">
                        </div>
                        </form>
                    </div>
                  </div>
                  {{-- end post with picture -------------------------------------------- --}}
                                       
                                    
                                  </div>
                               </div><!--/row-->
                              
                                <div class="row">
                                  <div class="col-sm-6">
                                    <a href="#">Twitter</a> <small class="text-muted">|</small> <a href="#">Facebook</a> <small class="text-muted">|</small> <a href="#">Google+</a>
                                  </div>
                                </div>
                              
                                <div class="row" id="footer">    
                                  <div class="col-sm-6">
                                    
                                  </div>
                                  <div class="col-sm-6">
                                    <p>
                                    <a href="#" class="pull-right">�Copyright 2013</a>
                                    </p>
                                  </div>
                                </div>
                              
                              <hr>
                              
                              <h4 class="text-center">
                              <a href="http://usebootstrap.com/theme/facebook" target="ext">Download this Template @Bootply</a>
                              </h4>
                                
                              <hr>
                                
                              
                            </div><!-- /col-9 -->
                        </div><!-- /padding -->
                    </div>
                    <!-- /main -->
                  
                </div>
            </div>
        </div>
    
    
        <!--post modal-->
        <div id="postModal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
          <div class="modal-dialog">
          <div class="modal-content">
              <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
                    Update Status
              </div>
              <div class="modal-body">
                  <form class="form center-block">
                    <div class="form-group">
                    <textarea class="form-control input-lg" autofocus="" placeholder="What do you want to share?" id="post_content"></textarea>
                    </div>
                  </form>
              </div>
              <div class="modal-footer">
                  <div>
                  <button class="btn btn-primary btn-sm" data-dismiss="modal" aria-hidden="true">Post</button>
                    <ul class="pull-left list-inline"><li><a href=""><i class="glyphicon glyphicon-upload"></i></a></li><li><a href=""><i class="glyphicon glyphicon-camera"></i></a></li><li><a href=""><i class="glyphicon glyphicon-map-marker"></i></a></li></ul>
                  </div>	
              </div>
          </div>
          </div>
        </div>
    </div>
    @endsection 
