@extends('layouts.app')

@section('content')
<div class="container">
      
    <div class="wrapper">
        <div class="box">
            <div class="row row-offcanvas row-offcanvas-left">
                <!-- main right col -->
                <div class="column col-sm-10 col-xs-11 col-lg-12" id="main">
                    
                    {{-- Top nav --}}
                  
                    <div class="padding">
                        <div class="full col-sm-9">
                           
                            <!-- content -->                      
                            <div class="row centerContent">
                              
                             <!-- main col left --> 
                             
                              
            <!-- main col right -->
            <div class="col-sm-6">
              <div id="updateStatus" class="well"> 
                <form class="form-horizontal" method="post" action="/storepost" enctype="multipart/form-data">
                    {{ csrf_field() }}
                 <h4>What's New</h4>
                 
                  <div class="form-group nomarg">
                   <textarea class="form-control" placeholder="Update your status" id="content" name="content">{{old('content')}}</textarea>
                 </div>
                <select class="form-control botMarg" placeholder="Category" name="category" id="category" required>
                  <option value="">Select Category</option>
                  <option value="c1">Feeling</option>
                  <option value="c2">Business</option>
                  <option value="c3">Life Style</option>
                  <option value="c4">Sport</option>
                  <option value="c5">General</option>
                </select >
                 <button class="btn btn-primary pull-right marBtn" type="submit" type="button">Post</button>
                 <label class="btn btn-primary pull-right marBtn">
                  Upload Picture
                <input id="uploadPhoto" name="mediapost" type="file" style="display: none;">
                </label>
                <ul class="list-inline"><li></li><li></li><li></li></ul>
                
                {{-- <select name="category" id="category">
                  <option value="">Select Category</option>
                  <option value="c1">Feeling</option>
                  <option value="c2">Business</option>
                  <option value="c3">Life Style</option>
                  <option value="c4">Sport</option>
                  <option value="c5">General</option>
                </select> --}}
                
          
                <img id="preview-image" src="img/150x150.gif" class=" pull-left img-preview noPreview"> 
               </form>
           </div>
            @foreach ($data as $post)
                
              {{-- post with picture --------------------------------------- --}}
              <div class="panel panel-default" id="post{{$post->id}}">
                @if($post->media_path)
                  <div class="panel-thumbnail"><img src="{{asset("/img/mediapost/".$post->media_path) }}" class="img-responsive image-wrapper"></div>
                @endif
                <div class="panel-body">
                    <div class="flexClss">
                        <img class="profilePic" src="img/150x150.gif" class="img-circle pull-left"> 
                        <h4>{{$post->user_name}}</h4>
                        @if($post->user_id == Auth::user()->id)
                        {{-- <button class="btn btn-danger delete-post" data-id="{{$post->id}}">Delete post</button> --}}
                        <button type="button" class=" btn btn-danger delete-post close positionRight" aria-label="Close" data-id="{{$post->id}}">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      @endif
                    </div>
                    <div class="clearfix"></div>
                      <hr>
                      {{-- @if($post->author_id)
                      <h6></h6>
                      @endif --}}
                    <p>{{$post->content}}</p>
                <hr>
                <a class="view-comment" href="" data-id={{$post->id}}>View comment</a>
                  <ul id="showComment{{$post->id}}"></ul>
                <hr>
                
                <div class="comment-section">
                  <div class="input-group">
                    <div class="input-group-btn">
                    <button class="btn btn-default likePost" id="likePost{{$post->id}}" data-post={{$post->id}}>
                      
                      <svg width="0.7em" height="0.7em" viewBox="0 0 16 16" class="bi bi-heart-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd" d="M8 1.314C12.438-3.248 23.534 4.735 8 15-7.534 4.736 3.562-3.248 8 1.314z"/>
                      </svg>
                        <span id="likeCount{{$post->id}}">
                          @if($post->like_count > 0)
                            {{$post->like_count}}
                          @endif
                        </span>  
                    </button>
                    @if($post->user_id != Auth::user()->id)
                      <button class="btn btn-default share-post" data-id={{$post->id}}><i class="glyphicon glyphicon-share"></i></button>
                    @endif
                    </div>
                  </div>
                  <form class="form-comment form-section" method="post" data-id="{{$post->id}}">
                  <input class="form-control" placeholder="Add a comment.." id="comment{{$post->id}}" type="text" data-id="{{$post->id}}" required>
                  <button class="btn btn-primary pull-right post-comment" data-id="{{$post->id}}">Post Comment</button></form>
                </div>
                  
                </div>

              </div>
              {{-- end post with picture -------------------------------------------- --}}

            @endforeach
            
                                  
              {{-- post with picture --------------------------------------- --}}
              <div class="panel panel-default">
                <div class="panel-thumbnail"><img src="img/bg_5.jpg" class="img-responsive"></div>
                <div class="panel-body">
                    <div class="flexClss">
                        <img class="profilePic" src="img/150x150.gif" class="img-circle pull-left"> 
                        <h4>Raayarn Ismail</h4>

                     </div>
                 
                  <hr>
                    <form>
                    <div class="input-group">
                      <div class="input-group-btn">
                      <button class="btn btn-default">+1</button><button class="btn btn-default"><i class="glyphicon glyphicon-share"></i></button>
                      </div>
                      <input class="form-control" placeholder="Add a comment.." type="text">
                    </div>
                    </form>
                </div>
              </div>
              {{-- end post with picture -------------------------------------------- --}}
                                   
                                
                              </div>
                           </div><!--/row-->
                          
                            <div class="row">
                              <div class="col-sm-6">
                                <a href="#">Twitter</a> <small class="text-muted">|</small> <a href="#">Facebook</a> <small class="text-muted">|</small> <a href="#">Google+</a>
                              </div>
                            </div>
                          
                            <div class="row" id="footer">    
                              <div class="col-sm-6">
                                
                              </div>
                              <div class="col-sm-6">
                                <p>
                                <a href="#" class="pull-right">�Copyright 2013</a>
                                </p>
                              </div>
                            </div>
                          
                          <hr>
                          
                          <h4 class="text-center">
                          <a href="http://usebootstrap.com/theme/facebook" target="ext">Download this Template @Bootply</a>
                          </h4>
                            
                          <hr>
                            
                          
                        </div><!-- /col-9 -->
                    </div><!-- /padding -->
                </div>
                <!-- /main -->
              
            </div>
        </div>
    </div>


    <!--post modal-->
    <div id="postModal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog">
      <div class="modal-content">
          <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
                Update Status
          </div>
          <div class="modal-body">
              <form class="form center-block">
                <div class="form-group">
                  <textarea class="form-control input-lg" autofocus="" placeholder="What do you want to share?"></textarea>
                </div> 
              </form>
          </div>
          <div class="modal-footer">
              <div>
              <button class="btn btn-primary btn-sm" data-dismiss="modal" aria-hidden="true">Post</button>
                <ul class="pull-left list-inline"><li><a href=""><i class="glyphicon glyphicon-upload"></i></a></li><li><a href=""><i class="glyphicon glyphicon-camera"></i></a></li><li><a href=""><i class="glyphicon glyphicon-map-marker"></i></a></li></ul>
              </div>	
          </div>
      </div>
      </div>
    </div>
    
</div>
@endsection