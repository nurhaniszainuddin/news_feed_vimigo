<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('css/facebook.css') }}" rel="stylesheet">
    <link href="{{ asset('css/newsfeed.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app" class="w-100">
        {{-- <nav class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse" aria-expanded="false">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->
                    <a class="navbar-brand" href="{{ url('/') }}">
                        {{ config('app.name', 'Laravel') }}
                    </a>
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav">
                        &nbsp;
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->
                        @guest
                            <li><a href="{{ route('login') }}">Login</a></li>
                            <li><a href="{{ route('register') }}">Register</a></li>
                        @else
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu">
                                    <li>
                                        <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav> --}}
        <!-- top nav -->
        <div class="navbar navbar-blue navbar-static-top">  
            <div class="navbar-header">
              <button class="navbar-toggle" type="button" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand logo">H</a>
            </div>
            <nav class="collapse navbar-collapse" role="navigation">
            <form class="navbar-form navbar-left">
                <div class="input-group input-group-sm" style="max-width:360px;">
                  <input class="form-control" placeholder="Search" name="srch-term" id="srch-term" type="text">
                  <div class="input-group-btn">
                    <button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></button>
                  </div>
                </div>
            </form>
            <ul class="nav navbar-nav">
                @guest
                
                    <li>
                        <a href="{{ route('register') }}" role="button" data-toggle="modal"><i class="glyphicon glyphicon-user"></i> Register</a> 
                    </li>
                    <li>
                        <a href="{{ route('login') }}" role="button" data-toggle="modal"><i class="glyphicon glyphicon-log-in"></i> Log In</a>
                    </li>
                @else
                    <li>
                        <a href="/mypost" role="button" data-toggle="modal"><i class="glyphicon glyphicon-user"></i>  My Post</a> 
                    </li>
                    <li>
                        <a href="#LogOutModal" role="button" data-toggle="modal"><i class="glyphicon glyphicon-log-out"></i> Log Out</a>
                    </li>
              @endguest
            </ul>
            <ul class="nav navbar-nav ">
            </ul>
            </nav>
        </div>
        <!-- /top nav -->
        <div id="LogOutModal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog">
              <div class="modal-content">
                  <div class="modal-header ">
                      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
                      Log Out
                  </div>
                  <div class="modal-body logout-section">
                    <p>Are you want to  log out ?</p>
                  </div>
                  <div class="modal-footer">
                      <div>
                      <button class="btn btn-primary btn-sm" data-dismiss="modal" aria-hidden="true" id="logout-btn">Yes</button>
                      {{-- <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout
                                        </a> --}}
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                      <button class="btn btn-primary btn-sm" data-dismiss="modal" aria-hidden="true">No</button>
                        <ul class="pull-left list-inline"><li></li><li></li><li></li></ul>
                      </div>	
                  </div>
              </div>
            </div>
          </div>

        @yield('content')
    </div>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="{{ asset('js/jquery.js') }}"></script>
    <script src="{{ asset('js/bootstrap.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function() {
			$('[data-toggle=offcanvas]').click(function() {
				$(this).toggleClass('visible-xs text-center');
				$(this).find('i').toggleClass('glyphicon-chevron-right glyphicon-chevron-left');
				$('.row-offcanvas').toggleClass('active');
				$('#lg-menu').toggleClass('hidden-xs').toggleClass('visible-xs');
				$('#xs-menu').toggleClass('visible-xs').toggleClass('hidden-xs');
				$('#btnShow').toggle();
			});
        });
    </script>
    <script>  
        $(document).ready(function() {
            // Pre-defined image for upload picture
            var brand = document.getElementById('uploadPhoto');
            if(brand){
                brand.className = 'attachment_upload';
                function readURL(input) {
                    if (input.files && input.files[0]) {
                        var reader = new FileReader();
                        
                        reader.onload = function(e) {
                            $('.img-preview').attr('src', e.target.result);
                            document.getElementById("preview-image").classList.add('previewImage');
                            document.getElementById("updateStatus").classList.add('addHeight');
                            
                        };
                        reader.readAsDataURL(input.files[0]);
                    }
                }
                $("#uploadPhoto").change(function() {
                    readURL(this);
                });
            }

            // Like post function
            function likePost(id) {
                var request = new XMLHttpRequest();
                try {
                    request.onreadystatechange = function() {
                        if (request.readyState == 4) {
                            var data = $.parseJSON(request.responseText);
                            if(data.status == 'Success'){
                                $( "#likeCount"+id ).html(data.like_count);
                            }
                        }
                    }//end of function  
                    request.open("GET", "/countevent/" + id +"/like_count", true);
                    request.send();
                } catch (e) {
                    alert("Unable to connect to server");
                    alert(e);     
                }    
            }

            $( ".likePost" ).click(function(e) {
                e.preventDefault();
                likePost($(this).attr('data-post'));
            });
            $( ".edit-post" ).click(function(e) {
                e.preventDefault();
                $post = $(this).attr('data-post');
                $('#post_content').val( $post );
            });

            //Delete post
            function deletePost(id) {
                var request = new XMLHttpRequest();
                try {
                    request.onreadystatechange = function() {
                        if (request.readyState == 4) {
                            var data = $.parseJSON(request.responseText);
                            if(data.status == 'Success'){
                                $("#post"+id).remove();
                                setTimeout(function(){ alert('Post successfully deleted.'); }, 500);  
                            }
                        }
                    }//end of function  
                    request.open("GET", "/deletepost/"+id, true);
                    request.send();
                } catch (e) {
                    alert("Unable to connect to server");
                    alert(e);     
                }   
            }
            $( ".delete-post" ).click(function(e) {
                e.preventDefault();
                deletePost($(this).attr('data-id')) 
            });
           
            function getCommentList(id) {
                
                var request = new XMLHttpRequest();
                try {
                    request.onreadystatechange = function() {
                        if (request.readyState == 4) {
                            var data = $.parseJSON(request.responseText);
                            if(data.status == 'Success'){
                                var container = $('#showComment'+id);
                                $.each(data.comment_list,function(i,item){
                                    container.append('<li class="list-group-item">'+item.comment+'</li>')
                                })
                            }
                        }
                    }; 
                    request.open("GET", "/comments/"+id, true);
                    request.send();
                } catch (e) {
                    alert("Unable to connect to server");
                    alert(e);     
                }    
            }

            function refreshComment(id) {
            $id = id;
                var container = $('#showComment'+$id);
                container.replaceWith('<ul id="showComment'+$id+'"></ul>');
                getCommentList($id);
           }
            function createPost(id,comment) {
                
                var request = new XMLHttpRequest();
                try {
                    request.onreadystatechange = function() {
                        if (request.readyState == 4) {
                            var data = $.parseJSON(request.responseText);
                            if(data.status == 'Success'){
                               var container =  $('#comment'+id).val('');
                                container.append('');
                                refreshComment(id);
                            }
                        }
                    };//end of function  
                    request.open("GET", "/storecomment/"+id+'/'+comment, true);
                    request.send();
                } catch (e) {
                    alert("Unable to connect to server");
                    alert(e);     
                }    
            }
            $( ".form-comment" ).submit(function(e) {
                e.preventDefault();
                $id = $(this).attr('data-id');
                createPost($id,$('#comment'+$id).val());
            });

            $( ".view-comment" ).click(function(e) {
                e.preventDefault();
                $id = $(this).attr('data-id');
                var container = $('#showComment'+$id);
                container.replaceWith('<ul id="showComment'+$id+'"></ul>');
                getCommentList($id);
                // $("showComment"+$id).toggle();

            });

            function sharePost(id) {
                
                var request = new XMLHttpRequest();
                try {
                    request.onreadystatechange = function() {
                        if (request.readyState == 4) {
                            var data = $.parseJSON(request.responseText);
                            console.log("YES HERE! ", data)
                            if(data.status == 'Success'){
                                // console.log("YES HERE!")
                                location.reload();
                            }
                        }
                    }; 
                    request.open("GET", "/sharepost/"+id, true);
                    request.send();
                } catch (e) {
                    alert("Unable to connect to server");
                    alert(e);     
                }    
            }
            $( ".share-post" ).click(function(e) {
                e.preventDefault();
                $id = $(this).attr('data-id');
                sharePost($id);
            });

            $( "#logout-btn" ).click(function(e) {
                e.preventDefault();
                $('#logout-form').submit();
            });
        });
            
    </script>
</body>
</html>
